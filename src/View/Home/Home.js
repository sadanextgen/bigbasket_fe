import React from 'react';
import { connect } from 'react-redux';

import Header from '../../Component/Header/Header';
import Products from '../../Component/Products/Products';
import { getProducts } from '../../Actions/product.action';



class Home extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            products: null
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("props----", nextProps.products)
        this.setState({ products: nextProps.products.data})
    }

    componentDidMount() {
        this.props.getProducts();
    }

    addToCart = (product) => {
        console.log("add to cart clicked", product)
    }

    render(){
        if(!this.state.products ) {
            return null;
          }
        return (
            <div>
                <Header />
                <Products 
                    products={this.state.products}
                    addToCart = {this.addToCart}
                />
                Home ..
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products.products,
        productsError: state.products.productsError,
        productsLoading: state.products.productsLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getProducts: () => dispatch(getProducts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
