import React from 'react';
import {
    Container, Col, Form,
    FormGroup, Label, Input,
    Button,
} from 'reactstrap';
import { Link } from 'react-router-dom'
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

import './Login.css'

const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    user(email: $email, password: $password) {
        userId
        firstName
        lastName
        email
        role
    }
  }
`;

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
        console.log("state----", this.state)
    }

    render() {
        const {email, password} = this.state;
        return (
            <Container className="App">
                <h2>Sign In</h2>
                <Form className="form">
                    <Col>
                        <FormGroup>
                            <Label>Email</Label>
                            <Input
                                type="email"
                                name="email"
                                id="exampleEmail"
                                placeholder="myemail@email.com"
                                onChange={(e) => this.handleChange(e)}

                            />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label for="examplePassword">Password</Label>
                            <Input
                                type="password"
                                name="password"
                                id="examplePassword"
                                placeholder="********"
                                onChange={(event) => this.handleChange(event)}
                            />
                        </FormGroup>
                    </Col>
                    <Mutation 
                        mutation={LOGIN} 
                        variables={{email, password}}
                        onCompleted={(data) => {
                            console.log("data----", data)
                            localStorage.setItem('userHash', JSON.stringify(data.user));
                            alert("Login Successful")
                            if(data.user.role === 'user') {
                                this.props.history.push('/home')
                            } else {
                                this.props.history.push('/admin')
                            }
                        }
                            
                        }
                    >
                        {(login) => (
                            <Button outline color="info" onClick={login}>Login</Button>
                    )}
                    </Mutation> 
                    <Link to="/sigunup">
                        <Button outline color="info" className="float-right">Sign Up</Button>
                    </Link>
    
                </Form>
            </Container>
        );
    }
    
}

export default Login;