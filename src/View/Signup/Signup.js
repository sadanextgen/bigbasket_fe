import React, { Component } from 'react';
import {
    Container, Col, Form,
    FormGroup, Label, Input,
    Button,
} from 'reactstrap';
import { Link } from 'react-router-dom'
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

import './Signup.css'

const SIGN_UP = gql`
  mutation signup($email: String!, $password: String!, $firstName: String!, $lastName: String!, $role: String!) {
    addUser(email: $email, password: $password, firstName: $firstName, lastName: $lastName, role: $role) {
        userId
        firstName
        lastName
        email
        role
    }
  }
`;

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            confirmPassword: ''
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
        console.log("state----", this.state)
    }

    render() {
        const { email, password, firstName, lastName} = this.state;
        let role = 'user';
        return (
            <Container className="App">
                <h2>Create Your Account</h2>
                <Form className="form">
                    <Col>
                        <FormGroup>
                            <Label>First name</Label>
                            <Input
                                type="text"
                                name="firstName"
                                id="firstName"
                                placeholder="John"
                                onChange={(e) => {this.handleChange(e)}}

                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Last Name</Label>
                            <Input
                                type="text"
                                name="lastName"
                                id="lastName"
                                placeholder="Doe"
                                onChange={(e) => {this.handleChange(e)}}
                            />
                        </FormGroup>
    
                        <FormGroup>
                            <Label>Email</Label>
                            <Input
                                type="email"
                                name="email"
                                id="exampleEmail"
                                placeholder="myemail@email.com"
                                onChange={(e) => {this.handleChange(e)}}
                            />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label for="password">Password</Label>
                            <Input
                                type="password"
                                name="password"
                                id="password"
                                placeholder="********"
                                onChange={(e) => {this.handleChange(e)}}
                            />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label for="confirmPassword">Password</Label>
                            <Input
                                type="password"
                                name="confirmPassword"
                                id="confirmPassword"
                                placeholder="********"
                                onChange={(e) => {this.handleChange(e)}}
                            />
                        </FormGroup>
                    </Col>
                    <Mutation 
                        mutation={SIGN_UP} 
                        variables={{email, password, firstName, lastName, role}}
                        onCompleted={(data) => {
                            console.log("data----", data)
                            localStorage.setItem('userHash', JSON.stringify(data.addUser));
                            alert("Signup Successful");
                            if(data.addUser.role === 'user') {
                                this.props.history.push('/home')
                            } else {
                                this.props.history.push('/admin')
                            }
                        }}
                    >
                        {(login) => (
                            <Button outline color="info" onClick={login}>Submit</Button>
                    )}
                    </Mutation> 
                    
                    <Link to="/login">
                        <Button outline color="info" className="float-right">Login</Button>
                    </Link>
    
                </Form>
            </Container>
        );
    }
    
}

export default Signup;