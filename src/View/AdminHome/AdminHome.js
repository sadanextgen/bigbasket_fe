import React from 'react';
import {
     Button
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Header from '../../Component/Header/Header';
import AdminProducts from '../../Component/Admin/Products.admin/Products.admin';
import { getProducts } from '../../Actions/product.action'

class AdminHome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: null
        }
    }

    componentWillReceiveProps(nextProps) {
        //set received props in state
        console.log("props----", nextProps.products)
        this.setState({ products: nextProps.products.data})
    }

    componentDidMount() {
        //fetch all products
        this.props.getProducts();
    }

    render(){
        if(!this.state.products ) {
            return null;
          }
        return (
            <div>
                <Header />
                <Link to={`/admin/createProduct`}>
                    <Button> Create new Product</Button>
                </Link>
                <AdminProducts products={this.state.products} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products.products,
        productsError: state.products.productsError,
        productsLoading: state.products.productsLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getProducts: () => dispatch(getProducts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminHome)
