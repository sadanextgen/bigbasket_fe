import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { createBrowserHistory } from "history";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { ApolloProvider } from 'react-apollo'
import ApolloClient from "apollo-boost";

import './index.css';
import reducers from './Reducers/index';
import Home from './View/Home/Home';
import Login from './View/Login/Login';
import Signup from './View/Signup/Signup'
import Logout from './Component/Logout/Logout';
import ProductInfo from './Component/ProductInfo/ProductInfo';
import AdminHome from './View/AdminHome/AdminHome';
import CreateProduct from './Component/Admin/CreateProduct/CreateProduct';
import OrderHistory from './Component/OrderHistory/OrderHistory';
import EditProduct from './Component/Admin/EditProduct/EditProduct';
import AdminProductInfo from './Component/Admin/ProductInfo.admin/ProductInfo.admin';

const client = new ApolloClient({
    uri: "http://localhost:4000/graphql"
  });
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const hist = createBrowserHistory();
const createStoreWithMiddleware = composeEnhancers(applyMiddleware(thunk, logger))(createStore);
const store = createStoreWithMiddleware(reducers);

const user = JSON.parse(localStorage.getItem('userHash'));

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      user && user.role === 'admin'
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
ReactDOM.render(
    <ApolloProvider client={client}>
        <Provider store={store}>
            <Router history={hist}>
                <div>          
                    <div>            
                        <Route exact path="/" component={Login} />
                        <Route exact path="/home" component={Home} />            
                        <Route exact path="/login" component={Login} /> 
                        <Route exact path="/sigunup" component={Signup} /> 
                        <Route exact path="/Logout" component={Logout} />
                        <Route exact path="/product/:id" component={ProductInfo} />
                        <Route exact path="/orderHistory" component={OrderHistory} />
                        <PrivateRoute exact path="/admin" component={AdminHome} />
                        <PrivateRoute exact path="/admin/product/:id" component={AdminProductInfo} />
                        <PrivateRoute exact path="/admin/createProduct" component={CreateProduct} />
                        <PrivateRoute exact path="/admin/editProduct/:id" component={EditProduct} />
                    </div>
                </div>
            </Router>
	    </Provider>
    </ApolloProvider>
	,
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA