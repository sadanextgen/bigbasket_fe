import { product } from '../Constants/product.constant';

const initialState = {
    products: [],
    productsLoading: false,
    productsError: null,

    product: [],
    productLoading: false,
    productError: null
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case product.GET_PRODUCTS_LOADING:
            return { ...state, productsLoading: true}
        case product.GET_PRODUCTS_SUCCESS:
            return { ...state, products: action.payload, productsLoading: false}
        case product.GET_PRODUCTS_FAILURE:
            return { ...state, products: [], productsError: action.payload, productsLoading: false}

        case product.FETCH_PRODUCT_LOADING:
            return { ...state, productLoading: true}
        case product.FETCH_PRODUCT_SUCCESS:
            return { ...state, product: action.payload, productLoading: false}
        case product.FETCH_PRODUCT_FAILURE:
            return { ...state, product: [], productError: action.payload, productLoading: false}

        default:
            return state;
    }
}

export default reducer;