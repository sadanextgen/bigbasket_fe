import { combineReducers } from 'redux';

import Products from './product.reducer';
import Cart from './cart.reducer';

const rootReducer = combineReducers({
    products: Products,
    cart: Cart
});

export default rootReducer;
