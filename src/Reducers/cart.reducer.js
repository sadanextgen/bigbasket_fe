import { cart } from '../Constants/cart.constant';

const initialState = {
    cart: [],
    cartLoading: false,
    cartError: null
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case cart.FETCH_CART_LOADING:
            return { ...state, cartLoading: true}
        case cart.FETCH_CART_SUCCESS:
            return { ...state, cart: action.payload, cartLoading: false}
        case cart.FETCH_CART_ERROR:
            return { ...state, cart: [], cartError: action.payload, cartLoading: false}

        default:
            return state;
    }
}

export default reducer;