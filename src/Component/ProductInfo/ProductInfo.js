import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Col, Row, Label, Input
} from 'reactstrap';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Link } from 'react-router-dom';

import Header from '../Header/Header'
const BUY_NOW = gql`
  mutation BuyNow($productId: ID!, $totalItem: Int!) {
    buyNow(productId: $productId, totalItem: $totalItem) {
        productId
    }
  }
`;

const ProductInfo = (props) => {
    console.log("info----", props.location.productInfo);
    let user = JSON.parse(localStorage.getItem('userHash'));
    let totalItem = 1;
    let productId = props.location.productInfo.productId;
    if(props.location && props.location.productInfo) {
        return (
            <div>
                <Header/>
                <Row>
                
                <Col sm="6" className="addSpace">
                    <Card>
                        <CardImg top width="100%" src={props.location.productInfo.imageUrl} alt="Card image cap" />
                    </Card>
                </Col>
                <Col sm="4" className="addSpace">
                    <Card>
                        <CardBody>
                            <CardTitle>{props.location.productInfo.title}</CardTitle>
                            <CardTitle>{props.location.productInfo.category}</CardTitle>
                            <CardSubtitle>{props.location.productInfo.price}</CardSubtitle>
                            <CardSubtitle>
                                Customer Rating : {props.location.productInfo.userRating}
                            </CardSubtitle>
                            <CardText>Available Items : {props.location.productInfo.availabeItem}</CardText>
                            <Label>Select No of Items</Label>
                            <Input 
                                type="number" 
                                name="availabeItem" 
                                id="availabeItem" 
                                onChange={(event) => {totalItem = event.target.value}}
                                min={1}
                                max={props.location.productInfo.availabeItem}
                            />
                            <CardText>
                                Description : {props.location.productInfo.description}
                            </CardText>
                            { user ? (
                                <Mutation 
                                mutation={BUY_NOW} 
                                variables={{productId, totalItem}}
                                onCompleted={() => {
                                    alert("Order placed successfully.")
                                    props.history.push('/home')
                                    }
                                    
                                }
                            >
                                {(buyNow) => (
                                <Button onClick={buyNow}> Buy Now  </Button>
                                )}
                            </Mutation>
                            )
                            :
                            (
                                <Link to={`/login`}>
                                    Please Login
                                </Link>
                            )
                        }
                            
                        </CardBody>
                    </Card>
                </Col>
            </Row>
       
            </div>
            )
    }
    else {
        return null;
    }
    
}

export default ProductInfo;