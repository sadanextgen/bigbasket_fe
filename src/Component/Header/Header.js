import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import './Header.css'
import Profile from '../Profile/Profile';
import Cart from '../Cart/Cart';


class Header extends React.Component {
    

    render() {
        let user = JSON.parse(localStorage.getItem('userHash'));
        if(user.role === 'user') {
            return(
                <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/home" className="logo"><b>Big-Basket</b></NavbarBrand>

                    <Collapse navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem className="activeMenu">
                                <NavLink href="/home" >Products</NavLink>
                            </NavItem>
                            <Cart />
                            <Profile />
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
            )
        } else if(user.role === 'admin') {
            return(
                <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/admin" className="logo"><b>Big-Basket</b></NavbarBrand>

                    <Collapse navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem className="activeMenu">
                                <NavLink href="/admin" >Products</NavLink>
                            </NavItem>
                            <Profile />
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
            )
        } else {
            return (
                <div>
                    <Navbar color="light" light expand="md">
                        <NavbarBrand href="/home" className="logo"><b>Big-Basket</b></NavbarBrand>
    
                        <Collapse navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem className="activeMenu">
                                    <NavLink href="/home" >Products</NavLink>
                                </NavItem>
                                <NavItem >
                                    <NavLink href="/login" >Login</NavLink>
                                </NavItem>
                                <Profile />
                            </Nav>
                        </Collapse>
                    </Navbar>
                </div>
            )
        }
        
    }
}

export default Header