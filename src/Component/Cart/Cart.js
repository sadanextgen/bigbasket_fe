import React from 'react';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchCart } from '../../Actions/cart.action';

let user = null;

class Cart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cart: null
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("next props----", nextProps);
        this.setState({cart: nextProps.cart.data})
    }

    componentDidMount() {
        user = JSON.parse(localStorage.getItem('userHash'));
        if(user) {
            this.props.fetchCart(user.userId);
        }
    }
    render() {
        let Item = null
        if(!user) {
            return null;
        }
        if(!this.state.cart) {
            return <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        Cart
                    </DropdownToggle>
                    <DropdownMenu right>
                        Loading...
                    </DropdownMenu>
                </UncontrolledDropdown>
        }
        if(this.state.cart && this.state.cart.cart) {
            let cart = this.state.cart.cart
             Item = (
                        <DropdownMenu right>
                            {cart.map((product, index) => {
                                return (
                                    <Link to={{ pathname: `/product/${product.productId}`, productInfo: product}}>
                                        <DropdownItem>
                                            {product.title}
                                        </DropdownItem>
                                    </Link>
                                    
                                )
                            })}
                        </DropdownMenu>
                        
                    )
        }
        return (
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                    Cart
                </DropdownToggle>
                {Item}
            </UncontrolledDropdown>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart.cart,
        cartError: state.cart.cartError,
        cartLoading: state.cart.cartLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCart: (id) => dispatch(fetchCart(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);