import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Col, Label
} from 'reactstrap';
import { Link } from 'react-router-dom';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

import './Product.css'

const ADD_TO_CART = gql`
  mutation addToCart($userId: ID!, $productId: ID!) {
    addProductToCart(userId: $userId, productId: $productId) {
        productId
    }
  }
`;



const Product = (props) => {
    console.log("product-----", props.product)
    let productId = props.product.productId;
    const user = JSON.parse(localStorage.getItem('userHash'));
    //console.log("user", user.userId, productId)
    let userId
    if(user) {
         userId = user.userId;
    }
    return (
        <Col sm="4" className="addSpace">
            <Card>
                <Link to={{ pathname: `/product/${props.product.productId}`, productInfo: props.product}}>
                    <CardImg top width="100%" src={props.product.imageUrl} alt="Card image cap" />
                </Link>
                
                <CardBody>
                    <CardTitle>{props.product.title}</CardTitle>
                    <CardSubtitle>{props.product.category}</CardSubtitle>
                    <CardText>Available Items : {props.product.availabeItem}</CardText>
                    {user ? (
                        <Mutation 
                        mutation={ADD_TO_CART} 
                        variables={{userId, productId}}
                        onCompleted={(data) => {
                            alert("Added Successfully");
                        }}
                    >
                        {(addToCart) => (
                            <Button className="float-right" onClick={addToCart}>Add To Cart</Button>
                    )}
                    </Mutation>
                    )
                    :
                    (
                        <Link to={`/login`}>
                            Please login
                        </Link>
                    )
                }
                     
                </CardBody>
            </Card>
        </Col>
    )
}

export default Product;