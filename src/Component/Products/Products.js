import React from 'react';
import Product from '../Product/Product';
import { Row } from 'reactstrap';


const Products = (props) =>{
    console.log("products----", props.products.products);
    if(props.products && props.products.products) {
        return (
            <Row>
              {props.products.products.map((product, index) => {
                  return (
                    <Product
                        product = {product}
                        addToCart = {props.addToCart}
                    />
                  )
              })}         
            </Row>
        )
    }
     else {
         return null;
     }
    
}

export default Products;