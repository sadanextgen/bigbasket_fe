import React from 'react';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import { Link } from 'react-router-dom';

const Profile = (props) => {
    let user = JSON.parse(localStorage.getItem('userHash'));
    if(user) {
        return (
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                    Hi {user? user.firstName : null}
                </DropdownToggle>
                <DropdownMenu right>
                    <DropdownItem>
                        <Link to="/logout">
                            Logout
                        </Link>
                    </DropdownItem>
    
                </DropdownMenu>
            </UncontrolledDropdown>
        )
    }
    return (
        <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav >
                    Hi 
                </DropdownToggle>
                
            </UncontrolledDropdown>
    )
    
}
export default Profile;