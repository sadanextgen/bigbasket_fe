import React from 'react';
import {
    Card, CardImg, CardBody,
    Button, Col, Row, Input,
 Form, FormGroup, Label
} from 'reactstrap';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const CREATE_PRODUCT = gql`
  mutation AddTodo(
      $category: String!, 
      $title: String!, 
      $description: String!,
      $manufacturer: String!, 
      $imageUrl: String!, 
      $userRating: Int!,
      $availabeItem: Int!,
      $price: Float!) {
    addProduct(category: $category, title: $title, description: $description,  manufacturer: $manufacturer, imageUrl: $imageUrl,  userRating: $userRating, availabeItem: $availabeItem, price: $price) {
        category
        title
        availabeItem
        imageUrl
        manufacturer
        userRating
        description
        price
    }
  }
`;


export default class CreateProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: '',
            title: '',
            imageUrl: '',
            manufacturer: '',
            userRating: 0,
            description: '',
            availabeItem: 0,
            price: 0
        }
    }

    

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value,
            [name + 'State']: "success"
        });
        console.log("state----", this.state)
    }

    render() {
        const { category, title, description, imageUrl, manufacturer, userRating, availabeItem, price} = this.state;
        return (
            <Row>
                <Col sm="6" className="addSpace">
                    <Card>
                        <CardImg top width="100%" src={this.state.imageUrl || "http://www.rangerwoodperiyar.com/images/joomlart/demo/default.jpg"} alt="Card image cap" />
                    </Card>
                </Col>
                <Col sm="4" className="addSpace">
                    <Card>
                        <CardBody>
                            <Form>
                                <FormGroup>
                                    <Label>Title</Label>
                                    <Input type="text" name="title" id="title" onChange={(event) => this.handleChange(event)} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Category</Label>
                                    <Input type="text" name="category" id="category" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Description</Label>
                                    <Input type="textarea" name="description" id="description" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Available Item</Label>
                                    <Input type="number" name="availabeItem" id="availabeItem" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label>User Rating</Label>
                                    <Input type="number" name="userRating" id="userRating" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Manufacturer</Label>
                                    <Input type="text" name="manufacturer" id="manufacturer" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Price</Label>
                                    <Input type="number" name="price" id="price" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Image url</Label>
                                    <Input type="text" name="imageUrl" id="imageUrl" onChange={(event) => this.handleChange(event)}/>
                                </FormGroup>
                                
                                <Mutation 
                                    mutation={CREATE_PRODUCT} 
                                    variables={{category, title, description, manufacturer, imageUrl, userRating, availabeItem, price}}
                                    onCompleted={() => this.props.history.push('/admin')}
                                >
                                    {(createProduct) => (
                                    <Button onClick={createProduct}>Submit</Button>
                                    )}
                                </Mutation>
                                
                            </Form>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }
}