import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Col
} from 'reactstrap';
import '../../Product/Product.css';
import { Link } from 'react-router-dom';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const DELETE_PRODUCT = gql`
  mutation DeleteProd($productId: ID!) {
    deleteProduct(productId: $productId) {
        productId
    }
  }
`;
const AdminProduct = (props) => {
    console.log("product-----", props.product)
    let productId = parseInt(props.product.productId);
    return (
        <Col sm="4" className="addSpace">
            <Card>
                <Link to={{pathname: `/admin/product/${props.product.productId}`, productInfo: props.product}}>
                    <CardImg top width="100%" src={props.product.imageUrl} alt="Card image cap" />
                </Link>
                
                <CardBody>
                    <CardTitle>{props.product.title}</CardTitle>
                    <CardSubtitle>{props.product.category}</CardSubtitle>
                    <CardText>Available Items : {props.product.availabeItem}</CardText>
                    <Link to={{ pathname: `/admin/editProduct/${props.product.productId}`, productInfo: props.product}}>
                        <Button> Edit </Button>
                    </Link>
                    <Mutation 
                        mutation={DELETE_PRODUCT} 
                        variables={{productId}}
                        onCompleted={() => {
                            alert("Item deleted successfully.")
                            props.history.push('/admin')
                            }
                            
                        }
                    >
                        {(deleteProduct) => (
                         <Button className="float-right" onClick={deleteProduct}> Delete </Button>
                        )}
                    </Mutation> 
                   
                </CardBody>
            </Card>
        </Col>
    )
}

export default AdminProduct;