import React from 'react';
import Header from '../Header/Header';
import AdminProducts from './Products.admin/Products.admin'
import {
     Button
} from 'reactstrap';
import { Link } from 'react-router-dom';

class Admin extends React.Component
{
    render(){
        return (
            <div>
                <Header />
                <Link to={`/admin/createProduct`}>
                    <Button> Create new Product</Button>
                </Link>
                
                <AdminProducts />
                Home ..
            </div>
        )
    }
}

export default Admin;
