import React from 'react';
import {
    Card, CardImg, CardBody,
    Button, Col, Row, Input,
 Form, FormGroup, Label
} from 'reactstrap';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { connect } from 'react-redux';
import { fetchProduct } from '../../../Actions/product.action';

const EDIT_PRODUCT = gql`
  mutation AddTodo(
      $productId: ID!,
      $category: String!, 
      $title: String!, 
      $description: String!,
      $manufacturer: String!, 
      $imageUrl: String!, 
      $userRating: Int!,
      $availabeItem: Int!,
      $price: Float!) {
    editProduct(productId: $productId, category: $category, title: $title, description: $description,  manufacturer: $manufacturer, imageUrl: $imageUrl,  userRating: $userRating, availabeItem: $availabeItem, price: $price) {
        category
        title
        availabeItem
        imageUrl
        manufacturer
        userRating
        description
        price
    }
  }
`;


class EditProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null,
            category: '',
            title: '',
            imageUrl: '',
            manufacturer:  '',
            userRating:  0,
            description: '',
            availabeItem:  0,
            price: 0
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("props----", nextProps.product)
        if(nextProps.product.data && nextProps.product.data.product) {
            let data = nextProps.product.data.product
            this.setState({
                category: data.category,
                title: data.title,
                imageUrl: data.imageUrl,
                manufacturer: data.manufacturer,
                userRating: data.userRating,
                description: data.description,
                availabeItem: data.availabeItem,
                productId: data.productId,
                price: data.price
            })
        }
        this.setState({ product: nextProps.product.data})
    }

    componentDidMount() {
        //extract id from params
        const id = this.props.match.params;
        //call product API
        this.props.fetchProduct(id.id);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value,
            [name + 'State']: "success"
        });
        console.log("state----", this.state)
    }

    render() {
        const {productId, category, title, description, imageUrl, manufacturer, userRating, availabeItem, price} = this.state;
        if(!this.state.product) {
            return null;
        }
        return (
            <Row>
                <Col sm="6" className="addSpace">
                    <Card>
                        <CardImg top width="100%" src={this.state.imageUrl || "http://www.rangerwoodperiyar.com/images/joomlart/demo/default.jpg"} alt="Card image cap" />
                    </Card>
                </Col>
                <Col sm="4" className="addSpace">
                    <Card>
                        <CardBody>
                            <Form>
                                <FormGroup>
                                    <Label>Title</Label>
                                    <Input 
                                        type="text" 
                                        name="title" 
                                        id="title" 
                                        value={title}
                                        onChange={(event) => this.handleChange(event)} 
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Category</Label>
                                    <Input 
                                    type="text" 
                                    name="category" 
                                    id="category" 
                                    value={category}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Description</Label>
                                    <Input 
                                    type="textarea" 
                                    name="description" 
                                    id="description" 
                                    value={description}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Available Item</Label>
                                    <Input 
                                    type="number" 
                                    name="availabeItem" 
                                    id="availabeItem" 
                                    value={availabeItem}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <FormGroup>
                                    <Label>User Rating</Label>
                                    <Input 
                                    type="number" 
                                    name="userRating" 
                                    id="userRating" 
                                    value={userRating}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Price</Label>
                                    <Input 
                                    type="number" 
                                    name="price" 
                                    id="price" 
                                    value={price}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Manufacturer</Label>
                                    <Input 
                                    type="text" 
                                    name="manufacturer" 
                                    id="manufacturer" 
                                    value={manufacturer}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Image url</Label>
                                    <Input 
                                    type="text" 
                                    name="imageUrl" 
                                    id="imageUrl" 
                                    value={imageUrl}
                                    onChange={(event) => this.handleChange(event)}
                                />
                                </FormGroup>
                                <Mutation 
                                    mutation={EDIT_PRODUCT} 
                                    variables={{productId, category, title, description, manufacturer, imageUrl, userRating, availabeItem, price}}
                                    onCompleted={() => this.props.history.push('/admin')}
                                >
                                    {(editProduct) => (
                                    <Button onClick={editProduct}>Submit</Button>
                                    )}
                                </Mutation> 
                            </Form>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        product: state.products.product,
        productError: state.products.productError,
        productLoading: state.products.productLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProduct: (id) => dispatch(fetchProduct(id))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProduct)