import React from 'react';
import { Row } from 'reactstrap';

import AdminProduct from '../Product.admin/Product.admin';

const AdminProducts = (props) => {
        console.log("products----", props.products.products);
        if(props.products && props.products.products) {
            return (
                <Row>
                  {props.products.products.map((product, index) => {
                      return (
                        <AdminProduct
                            product = {product}
                        />
                      )
                  })}         
                </Row>
            )
        }
         else {
             return null;
         }
}

export default AdminProducts;