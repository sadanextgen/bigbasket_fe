import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Col, Row
} from 'reactstrap';

const AdminProductInfo = (props) => {
    console.log("info----", props.location.productInfo);
    if(props.location && props.location.productInfo) {
        return (
            <Row>
                <Col sm="6" className="addSpace">
                    <Card>
                        <CardImg top width="100%" src={props.location.productInfo.imageUrl} alt="Card image cap" />
                    </Card>
                </Col>
                <Col sm="4" className="addSpace">
                    <Card>
                        <CardBody>
                            <CardTitle>{props.location.productInfo.title}</CardTitle>
                            <CardTitle>{props.location.productInfo.category}</CardTitle>
                            <CardSubtitle>{}</CardSubtitle>
                            <CardSubtitle>
                                Customer Rating : {props.location.productInfo.userRating}
                            </CardSubtitle>
                            <CardText>Available Items : {props.location.productInfo.availabeItem}</CardText>
                            <CardText>
                               Description:  {props.location.productInfo.description}
                            </CardText>
                            
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }
    else {
        return null;
    }
}

export default AdminProductInfo;