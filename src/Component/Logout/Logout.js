import React from 'react';
import {
    Container
} from 'reactstrap';

const Logout = () => {
    localStorage.removeItem('userHash')
    return (
        <Container className="App">
            <h2>You are Successfully Logged Out !</h2>
            <h4>Click below link to login again.</h4> <a href="/login">Login</a>           
        </Container>
    );
}

export default Logout;