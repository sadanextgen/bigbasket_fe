import gql from "graphql-tag";

import { client } from '../Config/graphQL.config';


export const login = (email, password) => {
    return (dispatch) => {
        client
            .query({
                query: gql`
                    {
                    user(email: ${email}, password: ${password}) {
                        userId
                        firstName
                    }
                }
                `
            })
            .then(data => {
                console.log("data---", data)
            })
            .catch(err => {
                console.log("error---", err)
            })
       }
}