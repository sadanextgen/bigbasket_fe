import gql from "graphql-tag";

import { client } from '../Config/graphQL.config';
import { product } from '../Constants/product.constant';

export const getProducts = () => {
    return (dispatch) => {
        dispatch({type: product.GET_PRODUCTS_LOADING})
        client
            .query({
                query: gql`
                    {
                    products {
                        productId
                        category
                        title
                        imageUrl
                        manufacturer
                        userRating
                        description
                        availabeItem
                    }
                }
                `
            })
            .then(data => {
                dispatch({
                    type: product.GET_PRODUCTS_SUCCESS,
                    payload: data 
                })
            })
            .catch(err => {
                dispatch({
                    type: product.GET_PRODUCTS_FAILURE,
                    payload: err 
                })
            })
    }
}

export const fetchProduct = (id) => {
    return (dispatch) => {
        dispatch({type: product.FETCH_PRODUCT_LOADING})
        client
            .query({
                query: gql`
                    {
                    product(productId: ${id}) {
                        productId
                        category
                        title
                        imageUrl
                        manufacturer
                        userRating
                        description
                        availabeItem
                    }
                }
                `
            })
            .then(data => {
                dispatch({
                    type: product.FETCH_PRODUCT_SUCCESS,
                    payload: data 
                })
            })
            .catch(err => {
                dispatch({
                    type: product.FETCH_PRODUCT_FAILURE,
                    payload: err 
                })
            })
        }
}
