import gql from "graphql-tag";

import { client } from '../Config/graphQL.config';
import { cart } from '../Constants/cart.constant';

export const fetchCart = (id) => {
    return (dispatch) => {
        dispatch({type: cart.FETCH_CART_LOADING})
        client
            .query({
                query: gql`
                    {
                    cart(userId: ${id}) {
                        productId
                        category
                        title
                        imageUrl
                        manufacturer
                        userRating
                        description
                        availabeItem
                    }
                }
                `
            })
            .then(data => {
                dispatch({
                    type: cart.FETCH_CART_SUCCESS,
                    payload: data 
                })
            })
            .catch(err => {
                dispatch({
                    type: cart.FETCH_CART_ERROR,
                    payload: err 
                })
            })
        }
}
