// import React, { Component } from 'react';
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import OrderHistory from './Component/OrderHistory/OrderHistory'
// import { faStar } from '@fortawesome/free-solid-svg-icons'
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
// import { createBrowserHistory } from "history";
// import { Provider } from 'react-redux';
// import { createStore, applyMiddleware, compose } from 'redux';
// import thunk from 'redux-thunk';
// import logger from 'redux-logger';

// import './App.css';
// import Home from './View/Home/Home';
// import Login from './Component/Login/Login';
// import Signup from './Component/Signup/Signup'
// import Logout from './Component/Logout/Logout';
// import ProductInfo from './Component/ProductInfo/ProductInfo';
// import Admin from './View/Admin/Admin';
// import CreateProduct from './Component/Admin/CreateProduct/CreateProduct';

// library.add([faStar])


// class App extends Component {
//   render() {
//     return (
//       <Router>
//         <div>          
//           <div>            
//             <Route exact path="/" component={Home} />
//             <Route exact path="/home" component={Home} />            
//             <Route exact path="/login" component={Login} /> 
//             <Route exact path="/sigunup" component={Signup} /> 
//             <Route exact path="/Logout" component={Logout} />
//             <Route exact path="/product/:id" component={ProductInfo} />
//             <Route exact path="/orderHistory" component={OrderHistory} />
//             <Route exact path="/admin" component={Admin} />
//             <Route exact path="/admin/createProduct" component={CreateProduct} />
//           </div>
//         </div>
//       </Router>
//     );
//   }
// }

// export default App;
